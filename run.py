#import os

from flask import render_template

from app import app

#config_name = os.getenv('FLASK_CONFIG','development')
#app = create_app(config_name)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

# Note - this error handler only works when debug mode is off; 
# to turn off debug mode, either change the config file or set FLASK_CONFIG=production
@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500

@app.errorhandler(403)
def forbidden_access(e):
    return render_template('403.html'), 403

if __name__ == '__main__':
    app.run()