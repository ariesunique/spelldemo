/*
delete from `spelldemo`.`attendance` where student_id > 0;
delete from `spelldemo`.`esl_class_student` where esl_class_id > 0;
delete from `spelldemo`.`esl_class_semester` where esl_class_id > 0;
delete from `spelldemo`.`esl_class` where esl_class_id > 0;
delete from `spelldemo`.`person_semester` where person_id > 0;
delete from `spelldemo`.`person` where person_id > 0;


# insert attendance code
INSERT INTO `spelldemo`.`attendance_code` (`code`,`description`) VALUES ('P', 'present');
INSERT INTO `spelldemo`.`attendance_code` (`code`,`description`) VALUES ('A', 'absent');
INSERT INTO `spelldemo`.`attendance_code` (`code`,`description`) VALUES ('H', 'half-day / left early');
INSERT INTO `spelldemo`.`attendance_code` (`code`,`description`) VALUES ('L', 'late');


# insert teachers and students
INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`email`,`phone`)
VALUES ('T','Aiyana','Brooks','ariesunique@gmail.com','646-260-4913');

INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`email`,`phone`)
VALUES ('T','Laura','Poe','laura.poe@gmail.com','718-123-4567');

INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`email`,`phone`)
VALUES ('T','Regina','Roberts','regina.roberts@ymail.com','347-917-3546');

INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`email`,`phone`)
VALUES ('T','Kristin','Summers','kristin.summers@gmail.com','123-456-7890');

INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`phone`)
VALUES ('S','Mickey','Mouse','718-098-1234');

INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`phone`)
VALUES ('S','Minnie','Mouse','212-098-1234');

INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`phone`)
VALUES ('S','Donald','Duck','917-098-1234');

INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`phone`)
VALUES ('S','Daisy','Duck','347-098-1234');

INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`phone`)
VALUES ('S','Scrooge','McDuck','404-098-1234');

INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`phone`)
VALUES ('S','Mary','Poppins','516-098-1234');

INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`phone`)
VALUES ('S','Michael','Banks','808-098-1234');

INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`phone`)
VALUES ('S','Jane','Banks','718-098-7777');

INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`phone`)
VALUES ('S','Emma','Swan','718-098-1111');

INSERT INTO `spelldemo`.`person` (`person_type`,`first_name`,`last_name`,`phone`)
VALUES ('S','Snow','White','718-098-2222');


# insert classes
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('1A','1');
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('1B','1');
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('2A','2');
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('2B','2');
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('3A','3');
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('3B','3');
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('4A','4');
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('4B','4');
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('5A','5');
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('5B','5');
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('6A','6');
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('6B','6');
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('BE1','Basic Education');
INSERT INTO `spelldemo`.`esl_class`(`esl_class_id`,`esl_level`) VALUES ('BE2','Basic Education');

INSERT INTO `spelldemo`.`esl_class_semester`(`esl_class_id`,`semester`,`room`,`morning_teacher_id`,`afternoon_teacher_id`)
VALUES ('1A','FA16','208 West',(select person_id from person where first_name='Laura'), (select person_id from person where first_name='Aiyana'));

INSERT INTO `spelldemo`.`esl_class_semester`(`esl_class_id`,`semester`,`room`,`morning_teacher_id`,`afternoon_teacher_id`)
VALUES ('1B','FA16','204 West',(select person_id from person where first_name='Aiyana'),(select person_id from person where first_name='Laura'));

INSERT INTO `spelldemo`.`esl_class_semester`(`esl_class_id`,`semester`,`room`,`morning_teacher_id`,`afternoon_teacher_id`)
VALUES ('2A','FA16','516 West',(select person_id from person where first_name='Regina'),(select person_id from person where first_name='Kristin'));

INSERT INTO `spelldemo`.`esl_class_semester`(`esl_class_id`,`semester`,`room`,`morning_teacher_id`,`afternoon_teacher_id`)
VALUES ('2B','FA16','1000 North',(select person_id from person where first_name='Kristin'),(select person_id from person where first_name='Regina'));

INSERT INTO `spelldemo`.`esl_class_student` (`esl_class_id`,`student_id`,`semester`)
VALUES ('1A',(select person_id from person where first_name='Mickey'),'FA16');

INSERT INTO `spelldemo`.`esl_class_student` (`esl_class_id`,`student_id`,`semester`)
VALUES ('1A',(select person_id from person where first_name='Minnie'),'FA16');

INSERT INTO `spelldemo`.`esl_class_student` (`esl_class_id`,`student_id`,`semester`)
VALUES ('1A',(select person_id from person where first_name='Donald'),'FA16');

INSERT INTO `spelldemo`.`esl_class_student` (`esl_class_id`,`student_id`,`semester`)
VALUES ('1A',(select person_id from person where first_name='Daisy'),'FA16');

INSERT INTO `spelldemo`.`esl_class_student` (`esl_class_id`,`student_id`,`semester`)
VALUES ('1B',(select person_id from person where first_name='Scrooge'),'FA16');

INSERT INTO `spelldemo`.`esl_class_student` (`esl_class_id`,`student_id`,`semester`)
VALUES ('1B',(select person_id from person where first_name='Snow'),'FA16');

INSERT INTO `spelldemo`.`esl_class_student` (`esl_class_id`,`student_id`,`semester`)
VALUES ('2A',(select person_id from person where first_name='Jane'),'FA16');

INSERT INTO `spelldemo`.`esl_class_student` (`esl_class_id`,`student_id`,`semester`)
VALUES ('2A',(select person_id from person where first_name='Mary'),'FA16');

INSERT INTO `spelldemo`.`esl_class_student` (`esl_class_id`,`student_id`,`semester`)
VALUES ('2A',(select person_id from person where first_name='Michael'),'FA16');

INSERT INTO `spelldemo`.`esl_class_student` (`esl_class_id`,`student_id`,`semester`)
VALUES ('2A',(select person_id from person where first_name='Emma'),'FA16');


INSERT INTO `spelldemo`.`semester_date` (`semester`,`session_num`,`cal_date`) VALUES('FA16',1,'2016-10-01');
INSERT INTO `spelldemo`.`semester_date` (`semester`,`session_num`,`cal_date`) VALUES('FA16',2,'2016-10-08');
INSERT INTO `spelldemo`.`semester_date` (`semester`,`session_num`,`cal_date`) VALUES('FA16',3,'2016-10-15');
INSERT INTO `spelldemo`.`semester_date` (`semester`,`session_num`,`cal_date`) VALUES('FA16',4,'2016-10-22');


# insert attendance data
INSERT INTO `spelldemo`.`attendance` (`student_id`,`semester_date_id`,`attendance_code`,`teacher_id`)
VALUES ((select person_id from person where first_name='Mickey'),(select semester_date_id from semester_date where semester='FA16' and session_num='1'),'P',(select person_id from person where first_name='Aiyana'));

INSERT INTO `spelldemo`.`attendance` (`student_id`,`semester_date_id`,`attendance_code`,`teacher_id`)
VALUES ((select person_id from person where first_name='Mickey'),(select semester_date_id from semester_date where semester='FA16' and session_num='1'),'P',(select person_id from person where first_name='Laura'));

INSERT INTO `spelldemo`.`attendance` (`student_id`,`semester_date_id`,`attendance_code`,`teacher_id`)
VALUES ((select person_id from person where first_name='Mickey'),(select semester_date_id from semester_date where semester='FA16' and session_num='2'),'P',(select person_id from person where first_name='Aiyana'));

INSERT INTO `spelldemo`.`attendance` (`student_id`,`semester_date_id`,`attendance_code`,`teacher_id`)
VALUES ((select person_id from person where first_name='Mickey'),(select semester_date_id from semester_date where semester='FA16' and session_num='2'),'P',(select person_id from person where first_name='Laura'));

INSERT INTO `spelldemo`.`attendance` (`student_id`,`semester_date_id`,`attendance_code`,`teacher_id`)
VALUES ((select person_id from person where first_name='Mickey'),(select semester_date_id from semester_date where semester='FA16' and session_num='3'),'P',(select person_id from person where first_name='Aiyana'));

INSERT INTO `spelldemo`.`attendance` (`student_id`,`semester_date_id`,`attendance_code`,`teacher_id`)
VALUES ((select person_id from person where first_name='Mickey'),(select semester_date_id from semester_date where semester='FA16' and session_num='3'),'P',(select person_id from person where first_name='Laura'));

INSERT INTO `spelldemo`.`attendance` (`student_id`,`semester_date_id`,`attendance_code`,`teacher_id`)
VALUES ((select person_id from person where first_name='Mickey'),(select semester_date_id from semester_date where semester='FA16' and session_num='4'),'P',(select person_id from person where first_name='Aiyana'));

INSERT INTO `spelldemo`.`attendance` (`student_id`,`semester_date_id`,`attendance_code`,`teacher_id`)
VALUES ((select person_id from person where first_name='Mickey'),(select semester_date_id from semester_date where semester='FA16' and session_num='4'),'P',(select person_id from person where first_name='Laura'));
*/
/******/

INSERT INTO `spelldemo`.`attendance` (`student_id`,`semester_date_id`,`attendance_code`,`teacher_id`)
VALUES (
	(select person_id from person where first_name='Minnie'),
    (select semester_date_id from semester_date where semester='FA16' and session_num='1'),
    'P',
    (select D.person_id from esl_class_student A, person B, esl_class_semester C, person D where A.semester = 'FA16' and A.student_id = B.person_id and B.first_name = 'Minnie' and A.esl_class_id = C.esl_class_id and C.morning_teacher_id = D.person_id)
);

INSERT INTO `spelldemo`.`attendance` (`student_id`,`semester_date_id`,`attendance_code`,`teacher_id`)
VALUES (
	(select person_id from person where first_name='Donald'),
    (select semester_date_id from semester_date where semester='FA16' and session_num='1'),
    'P',
    (select D.person_id from esl_class_student A, person B, esl_class_semester C, person D where A.semester = 'FA16' and A.student_id = B.person_id and B.first_name = 'Minnie' and A.esl_class_id = C.esl_class_id and C.morning_teacher_id = D.person_id)
);

INSERT INTO `spelldemo`.`attendance` (`student_id`,`semester_date_id`,`attendance_code`,`teacher_id`)
VALUES (
	(select person_id from person where first_name='Daisy'),
    (select semester_date_id from semester_date where semester='FA16' and session_num='1'),
    'P',
    (select D.person_id from esl_class_student A, person B, esl_class_semester C, person D where A.semester = 'FA16' and A.student_id = B.person_id and B.first_name = 'Minnie' and A.esl_class_id = C.esl_class_id and C.morning_teacher_id = D.person_id)
);

INSERT INTO `spelldemo`.`attendance` (`student_id`,`semester_date_id`,`attendance_code`,`teacher_id`)
VALUES (
	(select person_id from person where first_name='Scrooge'),
    (select semester_date_id from semester_date where semester='FA16' and session_num='1'),
    'P',
    (select D.person_id from esl_class_student A, person B, esl_class_semester C, person D where A.semester = 'FA16' and A.student_id = B.person_id and B.first_name = 'Minnie' and A.esl_class_id = C.esl_class_id and C.morning_teacher_id = D.person_id)
);


