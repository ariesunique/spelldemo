CREATE TABLE `attendance_code` (
   `code` char(1) NOT NULL,
   `description` varchar(45) NOT NULL,
   PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `language_code` (
   `code` char(4) NOT NULL,
   `description` varchar(45) NOT NULL,
   PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `country_code` (
   `code` char(4) NOT NULL,
   `description` varchar(45) NOT NULL,
   PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `users` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `username` varchar(60) NOT NULL,
   `email` varchar(45) NOT NULL,
   `password_hash` varchar(128) NOT NULL,
   `is_admin` tinyint(1) DEFAULT NULL,
   `person_id` int(11) NOT NULL, -- foreign key
   PRIMARY KEY (`id`),
   UNIQUE KEY `usertbl_email_unique_idx` (`email`),
   UNIQUE KEY `usertbl_username_unique_idx` (`username`),
   KEY `usertbl_person_fk_idx` (`person_id`),
   CONSTRAINT `usertbl_person_fk` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE NO ACTION ON UPDATE NO ACTION	   
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `person` (
  `person_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_type` char(1) NOT NULL, /* T=teacher, S=student, A=admin */
  `is_active` tinyint(1) DEFAULT 1 NOT NULL,  /* active or inactive */
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `middle_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,  /* 123-456-7890 */  
  `address` varchar(100) DEFAULT NULL,
  `language_code` char(4) DEFAULT NULL,  -- foreign key
  `country_code` char(4) DEFAULT NULL, -- foreign key   
  PRIMARY KEY (`person_id`),  
  CONSTRAINT `unique_person` UNIQUE (`last_name`, `first_name`, `email`, `phone`),
  INDEX `persontbl_name_idx` (`last_name`, `first_name`),
  INDEX `persontbl_email_idx` (`email`),
  INDEX `persontbl_type_idx` (`person_type`),
  KEY `persontbl_language_fk_idx` (`language_code`),
  KEY `persontbl_country_fk_idx` (`country_code`),    
  CONSTRAINT `persontbl_language_fk` FOREIGN KEY (`language_code`) REFERENCES `language_code` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `persontbl_country_fk` FOREIGN KEY (`country_code`) REFERENCES `country_code` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `person_semester` (	
	`person_id` int(11) NOT NULL, -- foreign key
	`semester` char(4) NOT NULL, /*e.g. FA16, SP17*/
	`status` char(1) NOT NULL,  -- N=new, R=returning
	`intake_score` int(11) DEFAULT NULL,
	`exit_score` int(11) DEFAULT NULL,	
    `hours` int(11) NOT NULL,  -- /*typically P,L=3, A=0, H=1.5; T may override and put any number */
    `num_absences` int(11) DEFAULT 0 NOT NULL, /* count num absences for morning attendance only; on the 5th absence mark student as inactive */	
	CONSTRAINT `personsemestertbl_pk` PRIMARY KEY (`person_id`,`semester`),
	INDEX `personsemestertbl_semester_idx` (`semester`),
	KEY `personsemestertbl_person_fk_idx` (`person_id`),
	CONSTRAINT `personsemestertbl_person_fk` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE NO ACTION ON UPDATE NO ACTION	
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `esl_class` (
  `esl_class_id` char(4) NOT NULL,		/* 1A, 1B, ... , BE1, BE2 */
  `esl_level` varchar(45) NOT NULL,       /*1, 2, 3, 4, 5, 6, Basic Education */
  PRIMARY KEY (`esl_class_id`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `esl_class_semester` (
  `esl_class_id` char(4) NOT NULL,	-- foreign key
  `semester` char(4) NOT NULL, /*e.g. FA16, SP17*/
  `room` varchar(45) NOT NULL,
  `morning_teacher_id` int(11) NOT NULL,	-- foreign key
  `afternoon_teacher_id` int(11) NOT NULL,	-- foreign key
  `notes` varchar(100) DEFAULT NULL,    
  CONSTRAINT `esl_class_semestertbl_pk` PRIMARY KEY (`esl_class_id`,`semester`),
  INDEX `esl_class_semestertbl_semester_idx` (`semester`),
  KEY `esl_class_semestertbl_teacher1_fk_idx` (`morning_teacher_id`),
  KEY `esl_class_semestertbl_teacher2_fk_idx` (`afternoon_teacher_id`),
  KEY `esl_class_semestertbl_class_fk_idx` (`esl_class_id`),
  CONSTRAINT `esl_class_semestertbl_teacher1_fk` FOREIGN KEY (`morning_teacher_id`) REFERENCES `person` (`person_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `esl_class_semestertbl_teacher2_fk` FOREIGN KEY (`afternoon_teacher_id`) REFERENCES `person` (`person_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,  
  CONSTRAINT `esl_class_semestertbl_class_fk` FOREIGN KEY (`esl_class_id`) REFERENCES `esl_class` (`esl_class_id`) ON DELETE NO ACTION ON UPDATE NO ACTION  
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `esl_class_student` (
  `esl_class_id` char(4) NOT NULL,  -- foreign key
  `student_id` int(11) NOT NULL,    -- foreign key
  `semester` char(4) NOT NULL, /*e.g. FA16, SP17*/
  CONSTRAINT `esl_class_studenttbl_pk` PRIMARY KEY (`esl_class_id`,`student_id`,`semester`),
  INDEX `esl_class_studenttbl_semester_idx` (`semester`),
  KEY `esl_class_studenttbl_class_fk_idx` (`esl_class_id`),
  KEY `esl_class_studenttbl_student_fk_idx` (`student_id`),
  CONSTRAINT `esl_class_studenttbl_class_fk` FOREIGN KEY (`esl_class_id`) REFERENCES `esl_class` (`esl_class_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `esl_class_studenttbl_student_fk` FOREIGN KEY (`student_id`) REFERENCES `person` (`person_id`) ON DELETE NO ACTION ON UPDATE NO ACTION  
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `attendance` (
  `student_id` int(11) NOT NULL, -- foreign key
  `semester_date_id` int(11) NOT NULL, -- foreign key
  `attendance_code` char(1) NOT NULL,  /* P=present, A=absent, H=half-day (left early), L=late*/
  `teacher_id` int(11) NOT NULL,	-- foreign key
  `notes` varchar(100) DEFAULT NULL,
  CONSTRAINT `attendancetbl_pk` PRIMARY KEY (`student_id`,`semester_date_id`,`teacher_id`),
  KEY `attendancetbl_semesterdate_fk_idx` (`semester_date_id`),
  KEY `attendancetbl_teacher_fk_idx` (`teacher_id`),
  KEY `attendancetbl_student_fk_idx` (`student_id`),
  CONSTRAINT `attendancetbl_teacher_fk` FOREIGN KEY (`teacher_id`) REFERENCES `person` (`person_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `attendancetbl_student_fk` FOREIGN KEY (`student_id`) REFERENCES `person` (`person_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,  
  CONSTRAINT `attendancetbl_semesterdate_fk` FOREIGN KEY (`semester_date_id`) REFERENCES `semester_date` (`semester_date_id`) ON DELETE NO ACTION ON UPDATE NO ACTION  
) ENGINE=InnoDB DEFAULT CHARSET=utf8

/*
need to add a program dates table to be able to display the program dates when teachers enter attendance
*/
CREATE TABLE `semester_date` (
   `semester_date_id` int(11) NOT NULL AUTO_INCREMENT,
   `semester` char(4) NOT NULL,
   `session_num` int(11) NOT NULL,  
   `cal_date` date NOT NULL,
   PRIMARY KEY (`semester_date_id`),
   CONSTRAINT `semesterdatetbl_unique` UNIQUE (`semester`,`session_num`),
   INDEX `semesterdatetbl_semester_idx` (`semester`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

/*
store the current semester (e.g. FA16); need to be able to cycle through to next semester
*/
CREATE TABLE `current_semester` (
	`semester` char(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8