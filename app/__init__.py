# third-party imports
from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect
from flask_debugtoolbar import DebugToolbarExtension

import os

# local imports
from config import app_config

# db variable initialization
db = SQLAlchemy()
login_manager = LoginManager()
csrf = CSRFProtect()

# Note: when I try to create a method create_app, the code breaks
#   investigate later
#def create_app(config_name):
config_name = os.getenv('FLASK_CONFIG', 'development')
app = Flask(__name__, instance_relative_config=True)
app.config.from_object(app_config[config_name])
app.config.from_pyfile('config.py')
#app.logger.setLevel(app_config[config_name].LOGLEVEL)

Bootstrap(app)  
db.init_app(app)
csrf.init_app(app) 

login_manager.init_app(app)
login_manager.login_message = "Please login before accessing other pages."
login_manager.login_view = "login"  

toolbar = DebugToolbarExtension(app)

from app import views
    
#    return app
    





