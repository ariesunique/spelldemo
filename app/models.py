from app import db, login_manager

from sqlalchemy import text, Column, Date, ForeignKey, Index, Integer, String, Table
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
        
        
Base = declarative_base()
metadata = Base.metadata


class Attendance(db.Model):
    __tablename__ = 'attendance'

    student_id = Column(ForeignKey(u'person.person_id'), primary_key=True, nullable=False, index=True)
    semester_date_id = Column(ForeignKey(u'semester_date.semester_date_id'), primary_key=True, nullable=False, index=True)
    attendance_code = Column(String(1), nullable=False)
    teacher_id = Column(ForeignKey(u'person.person_id'), primary_key=True, nullable=False, index=True)
    notes = Column(String(100))    

    semester_date = relationship(u'SemesterDate')
    student = relationship(u'Person', primaryjoin='Attendance.student_id == Person.person_id')
    teacher = relationship(u'Person', primaryjoin='Attendance.teacher_id == Person.person_id')
    
    def __init__(self, student_id, semester_date_id, attendance_code, teacher_id, notes):
        self.student_id = student_id
        self.semester_date_id = semester_date_id
        self.attendance_code = attendance_code
        self.teacher_id = teacher_id
        self.notes = notes

    def __repr__(self):
        return '<Attendance: On {}, {} was {}; Teacher: {}>'.format(self.semester_date.cal_date, self.student.first_name + " " + self.student.last_name, self.attendance_code, self.teacher.first_name)    


class AttendanceCode(db.Model):
    __tablename__ = 'attendance_code'

    code = Column(String(1), primary_key=True)
    description = Column(String(45), nullable=False)


class CountryCode(db.Model):
    __tablename__ = 'country_code'

    code = Column(String(4), primary_key=True)
    description = Column(String(45), nullable=False)


class CurrentSemester(db.Model):
    __tablename__ = 'current_semester'
    semester = Column(String(4), primary_key=True, nullable=False)
    
    def __init__(self, semester):
        self.semester = semester
    
    def __repr__(self):
        return '<CurrentSemester: {}>'.format(self.semester)

"""
t_current_semester = Table(
    'current_semester', metadata,
    Column('semester', String(4), nullable=False)
)
"""

class EslClas(db.Model):
    __tablename__ = 'esl_class'

    esl_class_id = Column(String(4), primary_key=True)
    esl_level = Column(String(45), nullable=False)


class EslClassSemester(db.Model):
    __tablename__ = 'esl_class_semester'

    esl_class_id = Column(ForeignKey(u'esl_class.esl_class_id'), primary_key=True, nullable=False, index=True)
    semester = Column(String(4), primary_key=True, nullable=False, index=True)
    room = Column(String(45), nullable=False)
    morning_teacher_id = Column(ForeignKey(u'person.person_id'), nullable=False, index=True)
    afternoon_teacher_id = Column(ForeignKey(u'person.person_id'), nullable=False, index=True)
    notes = Column(String(100))

    afternoon_teacher = relationship(u'Person', primaryjoin='EslClassSemester.afternoon_teacher_id == Person.person_id')
    esl_class = relationship(u'EslClas')
    morning_teacher = relationship(u'Person', primaryjoin='EslClassSemester.morning_teacher_id == Person.person_id')


class EslClassStudent(db.Model):
    __tablename__ = 'esl_class_student'

    esl_class_id = Column(ForeignKey(u'esl_class.esl_class_id'), primary_key=True, nullable=False, index=True)
    student_id = Column(ForeignKey(u'person.person_id'), primary_key=True, nullable=False, index=True)
    semester = Column(String(4), primary_key=True, nullable=False, index=True)

    esl_class = relationship(u'EslClas')
    student = relationship(u'Person')


class LanguageCode(db.Model):
    __tablename__ = 'language_code'

    code = Column(String(4), primary_key=True)
    description = Column(String(45), nullable=False)


class Person(db.Model):
    __tablename__ = 'person'
    __table_args__ = (
        Index('persontbl_name_idx', 'last_name', 'first_name'),
        Index('unique_person', 'last_name', 'first_name', 'email', 'phone', unique=True)
    )

    person_id = Column(Integer, primary_key=True)
    person_type = Column(String(1), nullable=False, index=True)
    first_name = Column(String(45), nullable=False)
    last_name = Column(String(45), nullable=False)
    middle_name = Column(String(45))
    email = Column(String(45), index=True)
    phone = Column(String(45))
    address = Column(String(100))    
    language_code = Column(ForeignKey(u'language_code.code'), index=True)
    country_code = Column(ForeignKey(u'country_code.code'), index=True)
    is_active = Column(Integer, nullable=False, server_default=text("'1'"))

    country_code1 = relationship(u'CountryCode')
    language_code1 = relationship(u'LanguageCode')
    
    def __init__(self, person_type, first_name, last_name, middle_name, email, phone, address):
        self.person_type = person_type
        self.first_name = first_name
        self.last_name = last_name
        self.middle_name = middle_name
        self.email = email
        self.phone = phone
        self.address = address
        
    
    def __repr__(self):
        return '<Person: {} {} - {}>'.format(self.first_name, self.last_name, self.person_type)


class PersonSemester(db.Model):
    __tablename__ = 'person_semester'

    person_id = Column(ForeignKey(u'person.person_id'), primary_key=True, nullable=False, index=True)
    semester = Column(String(4), primary_key=True, nullable=False, index=True)
    status = Column(String(1), nullable=False)
    intake_score = Column(Integer)
    exit_score = Column(Integer)
    hours = Column(Integer, nullable=False)
    num_absences = Column(Integer, nullable=False)

    person = relationship(u'Person')
    
    def calculate_hours(self, attendance_code):
        hour_values = {'P':3, 'L':2, 'H':1.5, 'A':0}


class SemesterDate(db.Model):
    __tablename__ = 'semester_date'
    __table_args__ = (
        Index('semesterdatetbl_unique', 'semester', 'session_num', unique=True),
    )

    semester_date_id = Column(Integer, primary_key=True)
    semester = Column(String(4), nullable=False, index=True)
    session_num = Column(Integer, nullable=False)
    cal_date = Column(db.DateTime, nullable=False)
    
    def __init__(self, semester, session_num, cal_date):
        self.semester = semester
        self.session_num = session_num
        self.cal_date = cal_date
    
    def __repr__(self):
        return '<SemesterDate: {} {} {}>'.format(self.semester, self.session_num, self.cal_date)
        
class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(60), nullable=False, unique=True)
    email = Column(String(45), nullable=False, unique=True)
    password_hash = Column(String(128), nullable=False)
    is_admin = Column(Integer)
    person_id = Column(ForeignKey(u'person.person_id'), nullable=False, index=True)

    person = relationship(u'Person')
    
    @property
    def password(self):
        """
        Prevent password from being accessed 
        """
        raise AttributeError("password is not a readable attribute")
        
    @password.setter
    def password(self, password):
        """
        Set password to a hashed password
        """
        self.password_hash = generate_password_hash(password)
        
    def verify_password(self, password):
        """
        Check if hased password matches actual password
        """
        return check_password_hash(self.password_hash, password)
    
    def __repr__(self):
        return '<User: {} {}>'.format(self.username, self.email)
        
# Set up user_loader
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))