from flask_wtf import FlaskForm
from wtforms import DateField, IntegerField, PasswordField, SelectField, StringField, SubmitField, ValidationError
from wtforms.fields import FieldList
from wtforms.validators import DataRequired, Email, EqualTo, Optional

from models import User

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    #email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')
    
class RegistrationForm(FlaskForm):
    person_id = IntegerField('PersonId', validators=[DataRequired()])
    username = StringField('Username', validators=[DataRequired()])    
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired(), EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('Repeat Password')
    submit = SubmitField('Register')
    
    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email is already in use.')
            

class PersonForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    middle_name = StringField('Middle Name')
    email = StringField('Email', validators=[Email(),Optional()])
    phone = StringField('Phone number')
    address = StringField('Address')
    language = StringField('Language')
    country = StringField('Country')
    submit = SubmitField('Submit')
    
    
class SemesterForm(FlaskForm):
    season = SelectField('Season', choices=[('WI','Winter'),('SP','Spring'),('SU','Summer'),('FA','Fall')], validators=[DataRequired()])
    year = SelectField('Year',choices=[('15','2015'),('16','2016'),('17','2017'),('18','2018'),('19','2019'),('20','2020')],validators=[DataRequired()])
    submit = SubmitField('Submit')
    

class SemesterDateForm(FlaskForm):
    dates = FieldList(DateField('Date', validators=[DataRequired()]), min_entries=6)             
    submit = SubmitField('Submit')