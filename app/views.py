from flask import abort, flash, redirect, render_template, request, url_for
from flask_login import login_required, login_user, logout_user, current_user
from urlparse import urlparse, urljoin

from app import app, db
from forms import LoginForm, RegistrationForm, PersonForm, SemesterDateForm, SemesterForm
from models import Attendance, CurrentSemester, EslClassSemester, EslClassStudent, Person, SemesterDate, User

import sys


morning = "morning"
afternoon = "afternoon"

@app.route('/')
@app.route('/index')
def index():
    return redirect(url_for("login"))


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data, password=form.password.data, person_id=form.person_id.data)
        db.session.add(user)
        db.session.commit()
        flash('User successfully registered!')
        
        return redirect(url_for('login'))
    
    return render_template('register.html', form=form)

    
@app.route('/login', methods=['GET', 'POST'])
def login():
    next_page = {'admin':'dashboard', 'teacher':'attendance'}
    
    if current_user and current_user.is_authenticated:
        if current_user.is_admin:
            return redirect(url_for(next_page['admin']))
        else:
            return redirect(url_for(next_page['teacher']))
        
    form = LoginForm()
    if form.validate_on_submit():
        # check whether employee exists in the database and whether
        # the password entered matches the password in the database
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user)
            # make sure that the "next" parameter is safe (ie, actually comes 
            # from this app and is not a parameter set by a malicious user)
            next_url = request.args.get("next")
            if not next_url or not is_safe_url(next_url):
                if user.is_admin:
                    return redirect(url_for(next_page['admin'])) 
                else:
                    return redirect(url_for(next_page['teacher']))
            else:
                return redirect(next_url)
        else:
            flash('Invalid username or password.')    
    return render_template('login.html', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have successfully logged out.')
    return redirect(url_for('login'))


def populate_person_from_form(person, form):
    person.first_name = form.first_name.data
    person.last_name = form.last_name.data
    person.email = form.email.data
    person.phone = form.phone.data
    person.address = form.address.data    
    return person
    
def populate_form_from_person(person, form):
    form.first_name.data = person.first_name
    form.last_name.data = person.last_name
    form.email.data = person.email
    form.phone.data = person.phone
    form.address.data = person.address
    return form

@app.route('/students')
@login_required
def students():
    students = []    
    if (current_user.person.person_type == 'T'):
        students = get_students(morning)
    elif (current_user.is_admin):
        students = Person.query.filter_by(person_type='S').order_by(Person.last_name).order_by(Person.first_name).all()
        
    return render_template("list.html", persons=students)

@app.route('/students/add', methods=['GET','POST'])
@login_required
def add_student():
    check_admin_access() 
    form = PersonForm()
    if form.validate_on_submit():
        student = Person("S", form.first_name.data, form.last_name.data, form.middle_name.data, form.email.data, form.phone.data, form.address.data)
        try:
            db.session.add(student)
            db.session.commit()
            flash("You have successfully added the student")
        except:
            print("Error", sys.exc_info()[0])
            flash("an error occurred")
    
        return redirect(url_for("students"))
        
    return render_template('person.html', form=form)

@app.route('/students/edit/<int:id>', methods=['GET','POST'])
@login_required
def edit_student(id):
    check_access(id)
    
    student = Person.query.get_or_404(id)
    form = PersonForm(obj=student)
    if form.validate_on_submit():
        student = populate_person_from_form(student, form)
        db.session.commit()
        flash("You have successfully edited the student")
        return redirect(url_for('students'))
    
    form = populate_form_from_person(student, form)
    return render_template('person.html', form=form, person=student)

@app.route('/teachers')
@login_required
def teachers():
    check_admin_access()    
    teachers = Person.query.filter_by(person_type='T').order_by(Person.last_name).order_by(Person.first_name).all()
    return render_template("list.html", persons=teachers)


@app.route('/teachers/add', methods=['GET','POST'])
@login_required
def add_teacher():
    check_admin_access() 
    form = PersonForm()
    if form.validate_on_submit():
        teacher = Person("T", form.first_name.data, form.last_name.data, form.middle_name.data, form.email.data, form.phone.data, form.address.data)
        try:
            db.session.add(teacher)
            db.session.commit()
            flash("You have successfully added the teacher")
        except:
            print("Error", sys.exc_info()[0])
            flash("an error occurred")
    
        return redirect(url_for("teachers"))
        
    return render_template('person.html', form=form)



@app.route('/teachers/edit/<int:id>', methods=['GET','POST'])
@login_required
def edit_teacher(id):
    check_admin_access()    
    teacher = Person.query.get_or_404(id)
    form = PersonForm(obj=teacher)
    if form.validate_on_submit():
        teacher = populate_person_from_form(teacher, form)
        db.session.commit()
        flash("You have successfully edited the teacher")
        return redirect(url_for('teachers'))
    form = populate_form_from_person(teacher, form)
    return render_template('person.html', form=form, person=teacher)


@app.route('/attendance', methods=['GET', 'POST'])
@login_required
def attendance():
    semester_date_id = None
    cal_date = None
    if request.method == 'POST':   
        print("HERE")
        if request.form['action']=='pickdate':
            print("Date selected ", request.form['form1-date']) 
            semester_date_id_str, cal_date = request.form['form1-date'].split("|")
            semester_date_id = int(semester_date_id_str)
        else:
            teacher_id = current_user.person.person_id
            morningnum = int(request.form['morningnum'])
            afternoonnum = int(request.form['afternoonnum'])
            student_ids = []
            for i in range(1,morningnum+1):
                student_ids.append(request.form['student'+str(i)+'M'])
            for i in range(1,afternoonnum+1):
                student_ids.append(request.form['student'+str(i)+'A'])
                
            for sid in student_ids:            
                att = request.form["optradio" + sid]
                attendance = Attendance.query.filter_by(student_id=sid).filter_by(teacher_id=current_user.person.person_id).filter_by(semester_date_id=1).first()
                print (attendance)
                if (attendance):
                    attendance.attendance_code = att
                    db.session.commit()
                else:
                    attendance = Attendance(sid, 1, att, teacher_id, "")
                    db.session.add(attendance)
                    db.session.commit()
    
    semester = CurrentSemester.query.first()  
    #semester_date = SemesterDate.query.filter_by(semester=semester.semester).filter_by(cal_date="2016-10-01").first()          
    semester_dates = SemesterDate.query.filter_by(semester=semester.semester).order_by(SemesterDate.session_num.desc()).all()          
    if not semester_date_id:
        selected_date = semester_dates[0]
        semester_date_id = selected_date.semester_date_id
        cal_date = selected_date.cal_date
        
    print("HERE, THERE, EVERYWHERE ", semester_date_id, cal_date)
    list1 = get_students(morning, semester_date_id)
    list2 = get_students(afternoon, semester_date_id)
    return render_template("attendance.html", morning=list1, afternoon=list2, semester=semester.semester, date=cal_date, semester_dates=semester_dates)

    
@app.route('/dashboard')
@login_required
def dashboard():            
    if current_user.is_admin:
        semester = CurrentSemester.query.first()
        return render_template('admin_dashboard.html', semester=semester)
        
    return render_template('dashboard.html')

"""
@app.route('/dashboard/admin')
@login_required
def admin_dashboard():            
    check_admin_access()     
    return render_template('admin.html')
"""

@app.route('/semester', methods=['GET', 'POST'])
@login_required
def semester():
    check_admin_access() 
    form = SemesterForm()
    curr_semester = CurrentSemester.query.first()
    
    if form.validate_on_submit():
        season = form.season.data
        year = form.year.data
        semester = season+year
        if (curr_semester):
            curr_semester.semester=semester
            db.session.commit()
        else:
            curr_semester = CurrentSemester(semester)
            db.session.add(curr_semester)
            db.session.commit()
        flash("You have successfully set the current semester")
        return redirect(url_for('dashboard'))            

    if (curr_semester):
        season=curr_semester.semester[0:2]
        year=curr_semester.semester[2:]
        form.season.data=season
        form.year.data=year

    return render_template('semester.html', form=form)
    
@app.route('/semester/dates', methods=['GET', 'POST'])
@login_required
def semester_dates():
    check_admin_access() 
    curr_semester = CurrentSemester.query.first()
    dates = SemesterDate.query.filter_by(semester=curr_semester.semester)
    form = SemesterDateForm()
    for itr, x in enumerate(dates):
        form.dates[itr].data = x.cal_date
    
    try:
        if form.validate_on_submit():
            SemesterDate.query.filter_by(semester=curr_semester.semester).delete()
            for itr, x in enumerate(form.dates.data):
                db.session.add(SemesterDate(curr_semester.semester, itr+1, x))            
            db.session.commit()
            return redirect(url_for('dashboard'))  
    except:
        flash("An exception occurred")
        print(sys.exc_info()[0])
        return redirect(url_for('dashboard'))  
    
    return render_template('semester_dates.html', form=form)
    

def check_access(sid):
    students = get_students(morning)
    students.extend(get_students(afternoon))
    sids = []
    if students:
        sids = [x.person_id for x in students]
    allowed = (current_user.is_admin) or (sid in sids)
    if not allowed:
        abort(403)
    
def check_admin_access():
    if not current_user.is_admin:
        abort(403)
    
def is_safe_url(target):
    # NOTE: this is slightly different in Python 3, so this would need to be updated
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and ref_url.netloc == test_url.netloc
    


def get_students(timeofday, date=None):    
    students = []    
    if (current_user.person.person_type == 'T'):
        teacher_id = current_user.person.person_id
        semester = CurrentSemester.query.first()
        
        if (timeofday==morning):
            spclass = EslClassSemester.query.filter_by(semester=semester.semester).filter_by(morning_teacher_id=teacher_id).first()
        elif (timeofday==afternoon):
            spclass = EslClassSemester.query.filter_by(semester=semester.semester).filter_by(afternoon_teacher_id=teacher_id).first()
            
        if (spclass):
            esl_class_students = EslClassStudent.query.filter_by(esl_class_id=spclass.esl_class_id).all()
            students = [x.student for x in esl_class_students]
            students.sort(key=lambda student: (student.last_name, student.first_name) )
            
            # FIX ME!!!!! -- get rid of hardcoding semester date
            if (date and students):                
                for student in students:
                    att = Attendance.query.filter_by(student_id=student.person_id).filter_by(teacher_id=teacher_id).filter_by(semester_date_id=date).first()
                    # dynamically add an attendance field to the student object so that we can easily access this on the view                                        
                    student.getField = lambda: None
                    if (att):
                        setattr(student.getField, "attendance", att.attendance_code)
                    else:
                        setattr(student.getField, "attendance", "A")  # default to absent if there is no attendance record for student
    return students
