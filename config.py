class Config(object):
    pass

class DebugConfig(Config):
    DEBUG = True
    SQLALCHEMY_ECHO = True    
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    
class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    
class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    
app_config = {
    'debug': DebugConfig,    
    'development': DevelopmentConfig,
    'production': ProductionConfig
}   